//
//  SchoolModels.swift
//  NYCSchools
//
//  Created by Arthi Palaniapan on 10/30/22.
//  Copyright © 2022 Arthi Palaniapan. All rights reserved.
//

import Foundation

struct AllSchools: Codable{
    var schools: [School]
}

struct School: Codable{
    
    let dbn: String
    let schoolName: String
    let overview: String?
    let boro: String?
    let neighborhood: String?
    let finalgrades: String?
    let website: String
    let email: String?
    let phoneNumber: String?
    let primaryAddress: String?
    let city: String?
    let zip: String?
    let latitude: String?
    let longitude: String?
    let borough: String?
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case overview = "overview_paragraph"
        case boro
        case neighborhood
        case finalgrades
        case website
        case email = "school_email"
        case phoneNumber = "phone_number"
        case primaryAddress = "primary_address_line_1"
        case city
        case zip
        case latitude
        case longitude
        case borough
    }
}

struct SchoolScore: Codable{
    let dbn: String
    let schoolName: String
    let testTakersCount: String
    let criticalReadingAvgScore: String
    let mathAvgScore: String
    let writingAvgScore: String
    
    enum CodingKeys: String,CodingKey {
        case dbn
        case schoolName = "school_name"
        case testTakersCount = "num_of_sat_test_takers"
        case criticalReadingAvgScore = "sat_critical_reading_avg_score"
        case mathAvgScore = "sat_math_avg_score"
        case writingAvgScore = "sat_writing_avg_score"
    }
}

enum SchoolResponseError: Error{
    case errorResponse
    case exception
    func errorMessage()->String{
        switch self {
        case .errorResponse:
            return "Please try later"
        case .exception:
            return "Unexpected Error Occured"
        }
    }
}
