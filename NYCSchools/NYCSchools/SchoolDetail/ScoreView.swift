//
//  ScoreView.swift
//  NYCSchools
//
//  Created by Arthi Palaniapan on 10/30/22.
//  Copyright © 2022 Arthi Palaniapan. All rights reserved.
//

import Foundation
import UIKit

class ScoreView: UIView{
    lazy var scoreLabel: UILabel = UILabel()
    lazy var scoreSubjectLabel: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupSubviews() {
        self.setupScoreLabel()
        self.setupScoreSubject()
        self.layer.cornerRadius = 10.0
        self.layer.borderWidth = 1.0
        self.accessibilityLabel = "\(scoreSubjectLabel.text ?? "") score \(scoreLabel.text ?? "")"
    }
    
    func setupConstraints() {
        setupScoreLabelConstraints()
        setupScoreSubjectConstraints()
    }
    
    func setupScoreLabelConstraints() {
        scoreLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let leadingConstraint = scoreLabel.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 0)
        let trailingConstraint = scoreLabel.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: 0)
        let topConstraint = scoreLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 10)
        let heightConstraint = scoreLabel.heightAnchor.constraint(equalToConstant: 40)
        self.addConstraints([leadingConstraint, trailingConstraint, topConstraint, heightConstraint])
    }
    
    func setupScoreSubjectConstraints() {
        scoreSubjectLabel.translatesAutoresizingMaskIntoConstraints = false

        let leadingConstraint = scoreSubjectLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0)
        let trailingConstraint = scoreSubjectLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0)
        let topConstraint = scoreSubjectLabel.topAnchor.constraint(equalTo: scoreLabel.bottomAnchor, constant: 10)
        let heightConstraint = scoreLabel.heightAnchor.constraint(equalToConstant: 40)
        self.addConstraints([leadingConstraint, trailingConstraint, topConstraint, heightConstraint])
        
    }
    
    func setupScoreLabel(){
        self.scoreLabel.font = UIFont.boldSystemFont(ofSize: 20.0)
        self.scoreLabel.textColor = .systemGreen
        self.scoreLabel.textAlignment = .center
        self.scoreLabel.text = "--"
        self.addSubview(scoreLabel)
    }
    
    func setupScoreSubject(){
        self.scoreSubjectLabel.font = UIFont.boldSystemFont(ofSize: 20.0)
        self.scoreSubjectLabel.textColor = .black
        self.scoreSubjectLabel.textAlignment = .center
        self.scoreSubjectLabel.text = "--"
        self.addSubview(scoreSubjectLabel)
    }
    
}
