//
//  SchoolScoreService.swift
//  NYCSchools
//
//  Created by Arthi Palaniapan on 10/30/22.
//  Copyright © 2022 Arthi Palaniapan. All rights reserved.
//

import Foundation

class SchoolScoreService: ServiceExecution {
    
    func initiateServiceCall(completionHandler: @escaping ((Result<Any, Error>) -> Void)) {
        guard let url = URL(string: self.endpoint) else {
            print("Invalid url")
            return
        }

        let dataTask = defaultDataSession.dataTask(with: url) {(data, response, error) in
            do {
                if let data = data{
                    let school = try JSONDecoder().decode([SchoolScore].self, from: data)
                    completionHandler(.success(school))
                }else{
                    completionHandler(.failure(SchoolResponseError.errorResponse))
                }
            }catch{
                print(error)
                completionHandler(.failure(SchoolResponseError.exception))
            }
        }
        dataTask.resume()
    }
    
    var endpoint: String {
        get {
            return "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        }
    }
    
}
