//
//  SchoolDetailViewModel.swift
//  NYCSchools
//
//  Created by Arthi Palaniapan on 10/30/22.
//  Copyright © 2022 Arthi Palaniapan. All rights reserved.
//

import Foundation

class SchoolDetailViewModel{
    /// The variable stores the scores of all schools
    var allSchoolScores: [SchoolScore]?{
        didSet {
            if let selectedSchool = selectedSchool{
              selectedSchoolScore = allSchoolScores?.filter({
                  return $0.dbn == selectedSchool.dbn
                }).first
            }
        }
    }

    /// School score of school selected from previous view
    var selectedSchoolScore: SchoolScore? {
        didSet{
            delegate?.handleSuccessResponse(operation: SchoolDetailViewOperations.updateUI)
        }
    }

    /// school selected from previous view
    var selectedSchool: School?

    //MARK:- Delegate and service variables
    weak var delegate: Bindable?
    var schoolScoreService: ServiceExecution?
    
    //MARK:- Initializers
    init(delegate: Bindable) {
        self.delegate = delegate
        self.schoolScoreService = SchoolScoreService()
    }
        
    //MARK:- Service calls
    /// TODO: We could cache this service response as the response is the same irrespective of the selected school
    func getSchoolDetail(){
        self.schoolScoreService?.initiateServiceCall(completionHandler: {[weak self] result in
            if let weakSelf = self{
                switch(result){
                case .failure(let error):
                    print(error.localizedDescription)
                    weakSelf.delegate?.handleFailureResponse(error: error ,operation: SchoolDetailViewOperations.updateUI)
                case .success(let schoolDetail):
                    weakSelf.allSchoolScores = schoolDetail as? [SchoolScore]
                }
            }
        })
    }
}
