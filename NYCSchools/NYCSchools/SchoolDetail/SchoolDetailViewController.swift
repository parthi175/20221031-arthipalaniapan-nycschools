//
//  SchoolDetailViewController.swift
//  NYCSchools
//
//  Created by Arthi Palaniapan on 10/28/22.
//  Copyright © 2022 Arthi Palaniapan. All rights reserved.
//

import UIKit

enum SchoolDetailViewOperations: Int {
    case updateUI
}

class SchoolDetailViewController: UIViewController {

    private var writingScore: ScoreView!
    private var readingScore: ScoreView!
    private var mathScore: ScoreView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var schoolOverview: UITextView!
    @IBOutlet weak var emailAddressLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    
    private var viewModel: SchoolDetailViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        getSchoolDetail()
    }
    
    //MAR:- View initial setup
    func setupView(){
        writingScore = ScoreView(frame: CGRect.zero)
        readingScore = ScoreView(frame: CGRect.zero)
        mathScore = ScoreView(frame: CGRect.zero)
        [writingScore, readingScore, mathScore].forEach {
            stackView.addArrangedSubview($0)
        }

        self.mathScore.scoreSubjectLabel.text = "Math"
        self.writingScore.scoreSubjectLabel.text = "Writing"
        self.readingScore.scoreSubjectLabel.text = "Reading"
    }
    
    func setViewModel(vm: SchoolDetailViewModel){
        viewModel = vm
    }
    
    func getSchoolDetail() {
        self.showActivityIndicator()
        viewModel.getSchoolDetail()
    }
    
    func showActivityIndicator(){
        self.activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator(){
        self.activityIndicator.stopAnimating()
    }
    
    func showAlert(message:String){
        let alert = UIAlertController(title: "Attention", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            alert.dismiss(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension SchoolDetailViewController: Bindable {
    func handleSuccessResponse<T>(operation: T) where T : RawRepresentable {
        DispatchQueue.main.async {
            self.hideActivityIndicator()
            guard let op = operation as? SchoolDetailViewOperations else {
                print("Invalid operation")
                return
            }
            switch op {
            case .updateUI:
                self.updateUIWithData()
            }
        }
    }
    
    /// TODO: Handle error response with better UX
    func handleFailureResponse<T>(error: Error, operation: T) where T : RawRepresentable {
        DispatchQueue.main.async {
            self.hideActivityIndicator()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    func updateUIWithData(){
        let selectedSchool = self.viewModel.selectedSchool
        let selectedSchoolScore = self.viewModel.selectedSchoolScore
        
        self.schoolName.text = selectedSchool?.schoolName ?? "--"
        self.schoolOverview.text = selectedSchool?.overview ?? "--"
        
        self.mathScore.scoreLabel.text = selectedSchoolScore?.mathAvgScore ?? "--"
        self.readingScore.scoreLabel.text = selectedSchoolScore?.criticalReadingAvgScore ?? "--"
        self.writingScore.scoreLabel.text = selectedSchoolScore?.writingAvgScore ?? "--"
        
        self.emailAddressLabel.text = selectedSchool?.email ?? "--"
        self.websiteLabel.text = selectedSchool?.website ?? "--"
    }
}
