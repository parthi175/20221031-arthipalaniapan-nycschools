//
//  ServiceExecution.swift
//  NYCSchools
//
//  Created by Arthi Palaniapan on 10/20/22.
//  Copyright © 2022 Arthi Palaniapan. All rights reserved.
//

import Foundation
import UIKit

protocol ServiceExecution {
    var endpoint: String { get }
    var defaultDataSession: URLSession { get }
    func initiateServiceCall(completionHandler: @escaping ((Result<Any, Error>)-> Void) )
}

extension ServiceExecution{
    var defaultDataSession: URLSession {
        get {
            let sessionConfig = URLSessionConfiguration.default
            return URLSession.init(configuration: sessionConfig)
        }
    }
}


