//
//  SchoolCell.swift
//  NYCSchools
//
//  Created by Arthi Palaniapan on 10/30/22.
//  Copyright © 2022 Arthi Palaniapan. All rights reserved.
//

import Foundation
import UIKit

class SchoolCell: UITableViewCell{
    
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var schoolCityLabel: UILabel!
    @IBOutlet weak var schoolBoroLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    func setupView()
    {
        self.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
    }
    
    func updateCellContent(school: School){
        self.schoolNameLabel.text = school.schoolName
        self.schoolBoroLabel.text = school.borough ?? "--"
        self.schoolCityLabel.text = school.city ?? "--"
    }
    
}
