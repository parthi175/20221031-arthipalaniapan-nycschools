//
//  SchoolListViewModel.swift
//  NYCSchools
//
//  Created by Arthi Palaniapan on 10/30/22.
//  Copyright © 2022 Arthi Palaniapan. All rights reserved.
//

import Foundation


class SchoolListViewModel{
   
    //MARK:- Class properties
    var schools: [School] = []
    var selectedSchool: School?

    //MARK:- Delegate and service variables
    var schoolListService: ServiceExecution?
    weak var delegate: Bindable?
    
    //MARK:- Initialzers
    init(schoolListService: ServiceExecution = SchoolListService()) {
        self.schoolListService = schoolListService
    }
    
    //MARK:- Service calls
    func getAllSchools(){
        self.schoolListService?.initiateServiceCall(completionHandler: { [weak self] result in
            if let weakSelf = self {
                switch result{
                case .failure(let err):
                    weakSelf.delegate?.handleFailureResponse(error: err, operation: SchoolListOperation.updateList)
                case .success(let school):
                    weakSelf.schools = school as! [School]
                    weakSelf.delegate?.handleSuccessResponse(operation: SchoolListOperation.updateList)
                }
            } else {
                fatalError("VC not present")
            }
        })
    }
}
