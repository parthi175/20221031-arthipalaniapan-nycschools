//
//  SchoolListViewController.swift
//  NYCSchools
//
//  Created by Arthi Palaniapan on 10/19/22.
//  Copyright © 2022 Arthi Palaniapan. All rights reserved.
//

import UIKit

enum SchoolListOperation: Int{
    case updateList = 0
}

protocol Bindable: AnyObject {
    func handleSuccessResponse<T: RawRepresentable>(operation: T)
    func handleFailureResponse<T:RawRepresentable>(error: Error, operation: T)
}

class SchoolListViewController: UIViewController{
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    //MARK:- Class variables
    private var viewModel = SchoolListViewModel()
   
    //MARK:- VC Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension

        viewModel.delegate = self
        getAllSchools()
    }
    
    func setupAccessibility(){
        self.view.accessibilityLabel = "School List"
    }
    
    //MARK:- Navigation methods
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier ==  "SchoolDetail", let vc = segue.destination as? SchoolDetailViewController {
            let schoolDetailViewModel = SchoolDetailViewModel(delegate: vc)
            schoolDetailViewModel.selectedSchool = viewModel.selectedSchool
            vc.setViewModel(vm: schoolDetailViewModel)
        }
    }
    
    func showActivityIndicator(){
        self.activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator(){
        self.activityIndicator.stopAnimating()
    }

    func getAllSchools(){
        showActivityIndicator()
        viewModel.getAllSchools()
    }
    
    /// Will show an alert message on the view, currently this will just display a message and dismiss on click of ok action
    /// This can be improved to take the handler blocks as input
    /// - Parameter message: The message which should be displayed on the alert
    //TODO: Improve this method so that it can be handled at one common place
    func showAlert(message:String){
        let alert = UIAlertController(title: "Attention", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            alert.dismiss(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

//MARK:- TableView delegates
extension SchoolListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let schoolCell: SchoolCell = tableView.dequeueReusableCell(withIdentifier: "schoolCell") as? SchoolCell {
            schoolCell.updateCellContent(school: viewModel.schools[indexPath.row])
            return schoolCell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectedSchool = viewModel.schools[indexPath.row]
        self.performSegue(withIdentifier: "SchoolDetail", sender: self)
    }
}

//MARK:- ViewModel bindings
extension SchoolListViewController: Bindable {
    
    //TODO: Add a better error UX with Retry option
    func handleFailureResponse<T>(error: Error, operation: T) where T : RawRepresentable {
        DispatchQueue.main.async{
            self.tableView.isHidden = true
            self.hideActivityIndicator()
            self.showAlert(message: "Get Schools list failed")
        }
    }
    
    func handleSuccessResponse<T>(operation: T) where T : RawRepresentable {
       let op = operation as? SchoolListOperation
        switch op {
        case .updateList:
            DispatchQueue.main.async {
                self.hideActivityIndicator()
                self.tableView.isHidden = false
                self.tableView.reloadData()
            }
        case .none:
            print("No case")
        }
    }
}

