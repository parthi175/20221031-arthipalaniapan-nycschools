//
//  _0221020_ArthiPalaniapan_NYCSchoolsTests.swift
//  NYCSchoolsTests
//
//  Created by Arthi Palaniapan on 10/19/22.
//  Copyright © 2022 Arthi Palaniapan. All rights reserved.
//

import XCTest
@testable import _0221020_ArthiPalaniapan_NYCSchools

class MockSchoolListService: ServiceExecution{
    var returnSuccessResponse: Bool = false
    
    func initiateServiceCall(completionHandler: @escaping ((Result<Any, Error>) -> Void)) {
        if returnSuccessResponse {
            let mockSchool = School(dbn: "dbn", schoolName: "Clinton School Writers & Artists, M.S. 260", overview: nil, boro: nil, neighborhood: nil, finalgrades: nil, website: "www.theclintonschool.net", email: nil, phoneNumber: nil, primaryAddress: nil, city: nil, zip: nil, latitude: nil, longitude: nil, borough: nil)
            completionHandler(.success([mockSchool]))
        } else {
            completionHandler(.failure(SchoolResponseError.errorResponse))
        }
        
    }
    
    var endpoint: String {
        "/mockEndpoint"
    }
}


class _0221020_ArthiPalaniapan_NYCSchoolsTests: XCTestCase {

    var schoolListViewModel: SchoolListViewModel?
    var service: MockSchoolListService?

    override func setUpWithError() throws {
        service = MockSchoolListService()
        schoolListViewModel = SchoolListViewModel(schoolListService: service!)
        try super.setUpWithError()
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
    }

    func testGetAllSchoolsSuccess() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        // Given
        service?.returnSuccessResponse = true
        
        // When
        schoolListViewModel?.getAllSchools()
        
        // Then
        XCTAssertTrue(schoolListViewModel?.schools.first?.dbn ?? "" == "dbn" )
    }
    
    func testGetAllSchoolsFailureInvalidResponse() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        // Given
        service?.returnSuccessResponse = false
        schoolListViewModel?.schools = []
        
        // When
        schoolListViewModel?.getAllSchools()
        
        // Then
        XCTAssertFalse(schoolListViewModel?.schools.first?.dbn ?? "" == "dbn" )
        XCTAssertTrue(schoolListViewModel?.schools.isEmpty ?? false)
    }

}
